#! /usr/bin/env python3

import datetime
import json
import os
import time

from bs4 import BeautifulSoup
import pync
import requests

ISOFORMAT = "%Y-%m-%dT%H:%M:%S%z"
NOTIFY_COUNT = 3

logo = os.path.join(os.path.dirname(os.path.abspath(__file__)), "guardian.png")

latest = datetime.datetime.fromtimestamp(0, datetime.timezone.utc)

# TODO: improve this logic
# index_url = datetime.date.today().strftime("https://www.theguardian.com/politics/blog/%Y/%b/%d/all")
index_url = (
    "https://www.theguardian.com/politics/series/politics-live-with-andrew-sparrow"
)

while True:
    previous_latest = latest

    # TODO optimise so this happens once a day.
    r = requests.get(index_url)
    assert r.ok

    # Get the full link to the first blog
    soup = BeautifulSoup(r.text, "html.parser")
    item = soup.find("a", attrs={"data-link-name": "article"})
    url = item.get("href")

    r = requests.get(url)
    assert r.ok
    soup = BeautifulSoup(r.text, "html.parser")
    for data in soup.find_all("script", type="application/ld+json"):
        j = json.loads(data.string)
        if j.get("@type", "") == "LiveBlogPosting":
            # Guardian appears to wrap the updates in a list, which looks to be
            # against spec?
            posts = j["liveBlogUpdate"][0]

            # Parse date strings
            for post in posts:
                post["datePublished"] = datetime.datetime.strptime(
                    post["datePublished"], ISOFORMAT
                )

            # Filter out posts we"ve seen already
            posts = (p for p in posts if p["datePublished"] > previous_latest)

            # Sort by date
            posts = sorted(posts, key=lambda p: p["datePublished"])

            # Show the latest NOTIFY_COUNT
            for post in posts[-NOTIFY_COUNT:]:
                latest = max(latest, post["datePublished"])

                # TODO this won"t show multiple notifications at once
                # TODO still has terminal icon/name
                pync.notify(
                    post["articleBody"],
                    title="Guardian Live Blog",
                    open=post["url"],
                    appIcon=logo,
                )

    time.sleep(5 * 60)
